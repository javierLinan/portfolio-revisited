# Portfolio revisited #

## Git workflow ##

- Ticket definition in the README file Changelog section.
- We create a branch for the ticket.
```
git checkout -b tXXXX
```
- We work on the new branch.
```
git checkout tXXXX
...
git commit -m "[Commit type] Brief explanation #XXXX"
...
```
- The new branch is merged with master without fast forward.
```
git checkout master
git merge --no-ff tXXXX
```

## Technology stack ##
- VueJS / Vuex / Vue-i18n / Vue-router
- ES6
- Karma
- Mocha
- Jade
- Materialize

## Commit types ##
- STYLE: Cosmetic changes (change file names, sort folders, etc).
- RE: Refactor changes.
- FE: New feature.
- RU: README file updated (Tickets added, new commit types, instructions, etc).
- FIX: Fixing an error or a wrong functionality.

## Changelog ##

- **t0001**: (New fetature) Internationalization support with translations splitted in each component.
- **t0002**: (New fetature) Language switcher for the site.
- **t0003**: (New fetature) App state store to save allowed languages, current language and other informations.
- **t0004**: (New fetature) The app index should initialize the app state and set the routing system.
- **t0005**: (New fetature) The language should be initialised from the user browser configuration and kept without using any parameter in the url.
- **t0006**: (New fetature) G basic design.
- **t0007**: (New fetature) Cv page.
- **t0008**: (New fetature) Contacts store with CRUD actions.
- **t0009**: (Refactor) The local storage handling should be removed from the mutations in appState store.
- **t0010**: (Refactor) Updating the content in the Cv page. Adding french translations.

## TODO ##
