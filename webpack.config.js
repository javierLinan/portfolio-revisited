var webpackConfigCommon = require("./webpack.config.common.js");
var path = require("path");
var merge = require("webpack-merge");
var webpack = require("webpack");
var TARGET = process.env.npm_lifecycle_event;

var PATHS = {
  app: path.join(__dirname, "src"),
  build: path.join(__dirname, "build")
};

var common = merge(webpackConfigCommon, {
  entry: {
    app: PATHS.app
  },
  output: {
    path: PATHS.build,
    filename: "bundle.js"
  }
});


// Default configuration
if(TARGET === "start" || !TARGET) {
  module.exports = merge(common, {
    devServer: {
      contentBase: PATHS.build,
      historyApiFallback: true,
      hot: true,
      inline: true,
      progress: true,
      stats: "errors-only",
      host: process.env.HOST,
      port: process.env.PORT //netstat -na | grep xxxx
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin()
    ],
    devtool: "eval-source-map"
  });
}

if(TARGET === "build") {
  module.exports = merge(common, {});
}
