import Vue from "vue"
import Vuex from "vuex"
import store from "store/store.js"
import merge from "merge"
import { bootstrap } from "../src/main.js"

export function createVm (options) {
  bootstrap(options.config, options.langcode)

  Vue.use(Vuex)

  const vm = new Vue(merge({
    store,
    replace: false
  }, options))

  vm.$mount()

  return vm
}
