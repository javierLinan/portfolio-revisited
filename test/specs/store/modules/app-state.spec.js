import {
  state,
  mutations
} from "store/modules/app-state.js"
import {
  initAppState,
  setAppState
} from "store/actions.js"
import store from "store/store.js"

describe("Vuex - App state store - Vuex.modules.appstate", () => {
  const {
    APP_STATE_INIT,
    APP_STATE_SET
  } = mutations
  const appState = {
    allowedLangs: ["en", "fr"],
    currentLang: "en"
  }
  describe("Mutations", () => {
    beforeEach(() =>
      APP_STATE_INIT(state, {})
    )
    it("should have an empty object for initial state", () => {
      expect(state).to.be.empty
    })
    it("APP_STATE_INIT", () => {
      APP_STATE_INIT(state, appState)
      expect(state).to.deep.equal(appState)
    })
    it("APP_STATE_SET", () => {
      APP_STATE_SET(state, "currentPage", "/home")
      expect(state).to.deep.equal({currentPage: "/home"})
    })
  })
  describe("Actions", () => {
    beforeEach(() =>
      initAppState(store, {})
    )
    describe("initAppState(store, appState)", () => {
      it("should throw an error when appState is not an object", () => {
        const fn = () => initAppState(store, "3")
        expect(fn).to.throw("The app state should be an state")
      })
      it("should replace state with appState when is an object", () => {
        initAppState(store, appState)
        expect(store.state.appState).to.deep.equal(appState)
      })
    })
    it("setAppState(store, key, value)", () => {
      setAppState(store, "currentPage", "/home")
      setAppState(store, "currentLang", "en")
      expect(store.state.appState).to.deep.equal({currentPage: "/home", currentLang: "en"})
    })
  })
})
