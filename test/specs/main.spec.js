import MainInjector from "inject!src/main.js"
import store from "store/store.js"
import { initAppState } from "store/actions.js"

describe("Application - Main", () => {
  describe("bootstrap(config, langcode)", () => {
    const config = {
      allowedLangs: ["en", "es"],
      defaultLang: "en"
    }
    let bootstrap = MainInjector({
      "store/store.js": store
    }).bootstrap

    afterEach(() => {
      initAppState(store, {})
    })

    it("should be stored the defaultLang as currentLang if the lang is not in the allowed langs", () => {
      bootstrap(config, "fr")
      expect(store.state.appState.currentLang).to.equal(config.defaultLang)
    })
    it("should be stored the correspondig lang as currentLang if it is in the allowed langs", () => {
      bootstrap(config, "es")
      expect(store.state.appState.currentLang).to.equal("es")
    })
    it("should been stored the allowed langs after bootstrap", () => {
      bootstrap(config, "es")
      expect(store.state.appState.allowedLangs).to.equal(config.allowedLangs)
    })
  })
})
