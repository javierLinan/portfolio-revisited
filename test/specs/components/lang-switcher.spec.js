import LangSwitcher from "components/langswitcher/lang-switcher.vue"
import { createVm } from "test/utils.js"
import store from "store/store.js"

describe("Language switcher - Components.LangSwitcher", () => {
  let vm
  let defaultLang = "en"

  afterEach(() => {
    vm.$destroy()
  })

  it("should render only the allowed languages", () => {
    let allowedLangs = ["en", "es", "fr"]

    vm = createVm({
      config: { allowedLangs, defaultLang },
      template: "<lang-switcher></lang-switcher>",
      components: { LangSwitcher }
    })

    expect(vm.$el.querySelectorAll("#language li.lang")).to.have.lengthOf(allowedLangs.length)

    for (let allowedLang of allowedLangs) {
      expect(vm.$el.querySelector("#language li.lang[data-lang='" + allowedLang + "']")).not.null
    }
  })

  it("should no render any language when the number of allowed languages is one", () => {
    let allowedLangs = ["en"]

    vm = createVm({
      config: { allowedLangs, defaultLang },
      template: "<lang-switcher></lang-switcher>",
      components: { LangSwitcher }
    })

    expect(vm.$el.querySelector("#language ul")).to.be.null
  })

  it("should be selected the link correspondig to the current language", () => {
    let allowedLangs = ["en", "es", "fr"]
    let langcode = "es"

    vm = createVm({
      config: { allowedLangs, defaultLang },
      langcode: langcode,
      template: "<lang-switcher></lang-switcher>",
      components: { LangSwitcher }
    })

    expect(vm.$el.querySelector("#language li.lang[selected][data-lang='" + langcode + "']")).not.null
  })

  it("should update the current lang in the app state store with the proper value when a switcher is clicked", () => {
    let allowedLangs = ["en", "es", "fr"]
    let nextLangcode = "es"

    vm = createVm({
      config: { allowedLangs, defaultLang },
      template: "<lang-switcher></lang-switcher>",
      components: { LangSwitcher }
    })

    let switcher = vm.$el.querySelector("#language li.lang[data-lang='" + nextLangcode + "']")
    switcher.click()
    expect(store.state.appState.currentLang).to.equal(nextLangcode)
  })
})
