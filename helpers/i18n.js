var glob = require("glob");
var YAML = require("yamljs");
var merge = require("merge");
var fs = require("fs");
var data = {};
var content = "";
var componentsPath = "src/components";
var assetsPath = "src/assets";

glob.sync("**/i18n.yml", {cwd: componentsPath}).forEach(function(i18nPath){
  i18nPath = componentsPath + "/" + i18nPath;
  try {
    var i18n = YAML.load(i18nPath);
  } catch (err) {
    console.log("Parsed error in " + i18nPath + ".");
    console.log(err);
  }

  if (i18n) {
    data = merge.recursive(true, data, i18n);
  }
});

try {
  content = YAML.stringify(data);
} catch(err){
  console.log("Error on creating i18n string from object.");
  console.log(err);
}

fs.writeFile(assetsPath + "/i18n.yml", content, function(err){
  if(err){
    console.log("Error on saving i18n file.");
    console.log(err);
  } else {
    console.log("Created i18n.yml file in \"" + assetsPath  + "\".");
  }
});
