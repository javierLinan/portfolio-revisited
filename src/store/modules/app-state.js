import {
  APP_STATE_INIT,
  APP_STATE_SET
} from "../mutation-types.js"
import Vue from "vue"
import { updateLocalStorage } from "helpers/utils.js"

const STORED_LOCAL = ["currentLang"]

// initial state
export const state = {}

// Mutations
export const mutations = {
  [APP_STATE_INIT] (state, appState) {
    for (let key in state) {
      Vue.delete(state, key)
    }
    for (let key in appState) {
      Vue.set(state, key, appState[key])
      if (STORED_LOCAL.indexOf(key) > -1) {
        updateLocalStorage(key, appState[key])
      }
    }
  },
  [APP_STATE_SET] (state, key, value) {
    if (state[key]) {
      state[key] = value
    } else {
      Vue.set(state, key, value)
    }

    if (STORED_LOCAL.indexOf(key) > -1) {
      updateLocalStorage(key, value)
    }
  }
}

export default {
  state,
  mutations
}
