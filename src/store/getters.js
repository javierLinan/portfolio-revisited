export const currentLang = (state) => {
  return state.appState.currentLang || state.appState.defaultLang
}
