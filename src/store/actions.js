import {
  APP_STATE_INIT,
  APP_STATE_SET
} from "./mutation-types.js"

export const initAppState = ({dispatch}, appState) => {
  if (!(typeof (appState) === "object" && appState.toString() === "[object Object]")) {
    throw new Error("The app state should be an state")
  }

  dispatch(APP_STATE_INIT, appState)
}
export const setAppState = ({dispatch}, key, value) => {
  dispatch(APP_STATE_SET, key, value)
}
