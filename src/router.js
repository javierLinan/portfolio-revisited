import Vue from "vue"
import VueRouter from "vue-router"
import Cv from "./components/cv/cv.vue"
import Component1 from "./components/component1/component1.vue"

Vue.use(VueRouter)

const router = new VueRouter()

const routes = [
  {
    "/": {
      name: "home",
      component: Cv
    }
  },
  {
    "/examples": {
      name: "component1",
      component: Component1
    }
  }
]

routes.forEach((route) => router.map(route))

router.redirect({"*": "/"})
router.beforeEach(() => window.scrollTo(0, 0))

export default router
