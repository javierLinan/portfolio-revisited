import Vue from "vue"
import I18n from "vue-i18n"
import { initFilters } from "./filters.js"
import {
  initAppState,
  setAppState
} from "store/actions.js"
import locales from "assets/i18n.yml"
import store from "store/store.js"
import router from "./router.js"
import App from "./components/app/app.vue"

export function bootstrap (config, langcode) {
  // ----------------------------------------
  // Debug
  // ----------------------------------------

  Vue.config.debug = true
  Vue.config.devtools = true

  // ----------------------------------------
  // Initialize the filters
  // ----------------------------------------

  initFilters()

  // ----------------------------------------
  // Initialize the app state
  // ----------------------------------------

  initAppState(store, config)

  let lang = config.defaultLang

  if (config.allowedLangs.indexOf(langcode) > -1) {
    lang = langcode
  }

  setAppState(store, "currentLang", lang)

  // ----------------------------------------
  // Internationalization
  // ----------------------------------------

  Vue.use(I18n, {
    lang: lang,
    locales: locales
  })
}

export function init () {
  // ----------------------------------------
  // Router
  // ----------------------------------------

  router.start(App, "#app")
}
