import Url from "url"

const STORAGE_KEY = "portfolio-revisited"

/**
 * getLangFromUrl - Parse a url to obtain the langcode contained
 *
 * @param  {String} urlStr The string with the url
 * @return {String}        The langcode found or an empty string in other case
 */
export function getLangFromUrl (urlStr) {
  const urlParsed = Url.parse(urlStr || "", true)
  return urlParsed.query.lang || ""
}

/**
 * getLangFromBrowser - Get the langcode from the browser parameters
 *
 * @return {String} The langcode found
 */
export function getLangFromBrowser () {
  return navigator.language || navigator.userLanguage
}

/**
 * getLangFromLocalStorage - Get the langcode from the local storage
 *
 * @return {String} The langcode stored
 */
export function getLangFromLocalStorage () {
  const data = fetchLocalStorage()
  return data.currentLang
}

export function fetchLocalStorage () {
  /* eslint-disable no-undef */
  return JSON.parse(localStorage.getItem(STORAGE_KEY) || "{}")
}

export function updateLocalStorage (key, value) {
  let data = fetchLocalStorage()
  if (data[key] !== value) {
    data[key] = value
    saveLocalStorage(data)
  }
}

export function saveLocalStorage (data) {
  /* eslint-disable no-undef */
  localStorage.setItem(STORAGE_KEY, JSON.stringify(data))
}
