import { bootstrap, init } from "./main.js"
import config from "./config.yml"
import {
  getLangFromBrowser,
  getLangFromLocalStorage
} from "./helpers/utils.js"

const lang = getLangFromLocalStorage() || getLangFromBrowser()
bootstrap(config, lang)
init()
