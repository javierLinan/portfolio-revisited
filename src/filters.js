import Vue from "vue"
import store from "store/store.js"
import { currentLang } from "store/getters.js"

export function initFilters () {
  Vue.filter("trans", function (value, arg) {
    return this.$t(value, currentLang(store.state), arg)
  })
}
