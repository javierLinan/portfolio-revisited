var webpackConfigCommon = require("./webpack.config.common.js");

module.exports = function (config) {
  config.set({
    browsers: ["PhantomJS"],
    frameworks: ["mocha", "chai-sinon"],
    reporters: ['mocha'],
    files: ["test/index.js"],
    preprocessors: {
      "test/index.js": ["webpack"]
    },
    webpack: webpackConfigCommon,
    webpackMiddleware: {
      noInfo: true
    },
    // singleRun: true
  })
};
