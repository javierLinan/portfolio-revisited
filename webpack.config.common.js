var path = require("path")

module.exports = {
  resolve: {
    extensions: ["", ".js", ".vue"],
    fallback: [path.join(__dirname, "node_modules")],
    alias: {
      "src": path.resolve(__dirname, "src"),
      "assets": path.resolve(__dirname, "src/assets"),
      "components": path.resolve(__dirname, "src/components"),
      "store": path.resolve(__dirname, "src/store"),
      "helpers": path.resolve(__dirname, "src/helpers"),
      "test": path.resolve(__dirname, "test")
    }
  },
  module: {
    preLoaders: [
      {
        test: /(\.js|\.vue)$/,
        exclude: /node_modules/,
        loader: "eslint"
      }
    ],
    loaders: [
      {
        test: /\.js$/, // include .js files
        exclude: /node_modules/, // exclude any and all files in the node_modules folder
        loader: "babel"
      },
      {
        test: /\.vue$/,
        exclude: /node_modules/,
        loader: "vue"
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: "url",
        query: {
          // limit for base64 inlining in bytes
          limit: 10000,
          // custom naming format if file is larger than
          // the threshold
          name: "[name].[ext]?[hash]"
        }
      },
      {
        test: /\.(yml|yaml)$/,
        exclude: /node_modules/,
        loader: "json!yaml",
      },
      {
        test: /\.scss$/,
        loaders: ["style", "css", "sass"]
      },
      {
        test: /\.css$/,
        loaders: ["style", "css", "sass?sourceMap"]
      },
      {
        test: /\.woff(\d*)\??(\d*)$/,
        loader: "url-loader?limit=10000&mimetype=application/font-woff"
      },
      {
        test: /\.ttf\??(\d*)$/,
        loader: "file-loader"
      },
      {
        test: /\.eot\??(\d*)$/,
        loader: "file-loader"
      },
      {
        test: /\.svg\??(\d*)$/,
        loader: "file-loader"
      }
    ]
  },
  vue: {
    autoprefixer: {
      browsers: ["last 2 versions"]
    }
  }
};
